## Jupyter Notebooks ##

This directory holds any Jupyter Notebooks created for visualizing logs generated by the scripts in [co2-sensor-display](https://gitlab.com/cordelya/co2-sensor-display).

Use the plain log files, not the ones generated for Grafana mysql importing.

[How to Use Jupyter Notebooks](https://www.dataquest.io/blog/jupyter-notebook-tutorial/)

### Run it Locally ###

This assumes that you have a Python environment and terminal access. If not, you can try out the Binder option in the next section.

* Install requirements:
```sh
pip install -r requirements.txt
```
* Start the Jupyter server.
* Navigate to the Jupyter web interface.
* Open the Introduction Notebook.
* It will walk you through the process.

### Try it in a Binder ###

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/cordelya%2Fco2-sensor-notebooks/main)

Click on the Binder button above and a new tab or window will open. It will take a minute for the instance to start up.
Once it is done loading, you will have a personal sandbox containing the contents of this repository that you may use as you like. 

Make sure you download anything you want to keep - after the instance is shut down, it goes away forever.
